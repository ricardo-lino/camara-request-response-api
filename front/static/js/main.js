const API_URL = ''

function sendData() {
    const data = getFormData()
    console.log(data)
    fetch(`${API_URL}/`, {
            method: "POST",
            body: data
        })
        .then((response) => {
            console.log(response)
        })
        .catch(err => console.log(err));
}

function getFormData() {
    return {
        name: $('.form input[name="name"]').val(),
        email: $('.form input[name="email"]').val(),
        cpf: $('.form input[name="cpf"]').val(),
        description: $('.form textarea[name="description"]').val(),
    }
}