import React, { Component } from "react";
import Question from "./Question";

class Questions extends Component {
  render() {
    return (
      <div>
        {this.props.questions.map(question => (
          <Question
            key={question._id.$oid}
            question={question}
            questionClick={id => this.props.questionClick(id)}
          />
        ))}
      </div>
    );
  }
}

export default Questions;
