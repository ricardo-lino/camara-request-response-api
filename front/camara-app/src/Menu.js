import React, { Component } from "react";

class Menu extends Component {
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
        {/* eslint-disable-next-line*/}
        <a className="navbar-brand" href="#">
          Pedidos de informação
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              {/* eslint-disable-next-line*/}
              <a className="nav-link" href="#">
                Inicio
              </a>
            </li>
            <li className="nav-item">
              {/* eslint-disable-next-line*/}
              <a className="nav-link" href="#">
                Ajuda
              </a>
            </li>
            <li className="nav-item float-right">
              {/* eslint-disable-next-line*/}
              <a className="nav-link" href="#">
                Sobre
              </a>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

export default Menu;
