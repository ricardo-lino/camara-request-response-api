import React, { Component } from "react";
import "./App.css";
import Home from "./Home";
import Menu from "./Menu";
class App extends Component {
  render() {
    return (
      <div>
        <div>
          <Menu />
        </div>
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <Home />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
