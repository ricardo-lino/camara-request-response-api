import React, { Component } from "react";
import { API_URL } from "./API";

class QuestionForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      author: "",
      email: "",
      description: "",
      cpf: ""
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.sendQuestion = this.sendQuestion.bind(this);
  }
  sendQuestion() {
    fetch(`${API_URL}/question`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(this.state)
    }).then(() => {
      this.props.getQuestions();
    });
  }
  handleInputChange(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <div className="row">
        <div className="col-md-8 offset-md-2">
          <div className="form">
            <div className="row">
              <div className="col-md-12">
                <div className="alert alert-info" role="alert">
                  Preencha os campos abaixo para enviar um pedido de informação
                  para a Câmara de Vereadores de São Paulo
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12">
                <div className="form-group">
                  <label>Nome</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Nome"
                    name="author"
                    onChange={this.handleInputChange}
                  />
                </div>
                <div className="form-group">
                  <label>E-mail</label>
                  <input
                    type="email"
                    className="form-control"
                    placeholder="E-mail"
                    name="email"
                    onChange={this.handleInputChange}
                  />
                </div>
                <div className="form-group">
                  <label>CPF</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="CPF"
                    name="cpf"
                    onChange={this.handleInputChange}
                  />
                </div>
                <div className="form-group">
                  <label>Descreva seu pedido</label>
                  <textarea
                    className="form-control"
                    rows="3"
                    placeholder="Descreva seu pedido..."
                    name="description"
                    onChange={this.handleInputChange}
                  />
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12">
                <button
                  type="button"
                  className="btn btn-primary float-right"
                  onClick={this.sendQuestion}
                >
                  Enviar
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default QuestionForm;
