import React, { Component } from "react";
import "./Question.css";
class Question extends Component {
  componentDidMount() {}
  parseDate(date) {
    date = new Date(date);
    return `${date.getDate()}/${date.getMonth() + 1}/${date.getYear() + 1900}`;
  }
  render() {
    let status = this.props.question.status ? (
      <i className="fas fa-check" />
    ) : (
      <i className="far fa-clock" />
    );
    return (
      <div
        className="question"
        onClick={id =>
          !this.props.disableClick
            ? this.props.questionClick(this.props.question._id["$oid"])
            : ""
        }
      >
        <div className="item">
          <div className="status">{status}</div>
        </div>
        <div className="item">
          <div className="description">{this.props.question.description}</div>
          <div className="author">{this.props.question.author}</div>
        </div>
        <div className="created">
          {this.parseDate(this.props.question.created["$date"])}
        </div>
      </div>
    );
  }
}

export default Question;
