# Governo Aberto - Atas

## 27/11/2018
Participantes: Edison, Ricardo e Yago
Apresentação do Trabalho Final


## 20/11/2018
Participantes: Edison, Ricardo e Yago
Levantamento dos pontos para finalizar o desenvolvimento do projeto, levantamento das informações para finalizar o relatório.

## 13/11/2018
Participantes: Ricardo e Yago
Apresentação parcial do trabalho
Alinhamento do front feito pelo Edison
Definição da arquitetura do api
Edsion ficará de fazer o vinculo com o banco de dados e fazer a comunicação entre o front e o backend 


## 06/11/2018
Participantes: Edison, Ricardo e Yago(via call, faltamos nesse dia)
Levantamentos dos impeditivos com a api do google, pois as respostas vem em abas diferente dentro do email
Foi feita uma lógica para retirar a resposta que precisará ser revista numa segunda versão da API
Foi feito alguns levantamentos bibliográficos ainda faltantes para um melhor desenvolvimento de relatório 


## 30/10/2018
Participantes: Ricardo e Yago
Leitura da bibliografia necessária, professora ficou de enviar email de novos bibliografias
Alinhamento do desenvolvimento da API



## 23/10/2018
Participantes: Edison, Ricardo e Yago

Discussão da bibliografia, estrutura do trabalho e criação do servidor em pytohn.

O que foi gerado hoje:

1. Introdução 
    - Breve conceitualização de controle social.
    - Relação controle social e T.I e o papel dos dados abertos (textos Gisele e Machado)
    - Papel do "Cuidando do Meu Bairro"
    - Resumo dos capítulos

2. Objetivos
    - Divulgar papel do vereador na fiscalização das obras públicas e do executivo.
    - Criar instrumento que facilite a comunicação do cidadão com o vereador.

3. Metodologia
    - Revisão bibliográfica(google acadêmico: dados abertos, legislativo, ouvidoria e transparência)
        - Papel do legislativo
        - Canais de comunicação
            - Jornal/mídia
            - Transparência passiva
            - Transparência alternativa
            - Direto com vereador
            - Dados abertos
        - Controle social/fiscalização
    - Arquitetura do programa
    - Etapas do desenvolvimento
    
4. Resultados
 
5.  Discussão
    - Problemas encontrados durante o trabalho
    - Desafios
    - Possibilidades
        - Integração com o "Cuidando do Meu Bairro" 




## 16/10/2018
Participantes: Edison, Ricardo e Yago

Discussão das tecnologias que iremos utilizar e levantamentos bibliográficos.

Discussão da definição de estrutura do backend e do frontend que irá consumir a api. Dividimos a estrutura do backend em duas partes:

    - API:  Iremos criar um serviço onde poderemos: realizar pergunta, listar perguntas e consultar pergunta(detalhe).

    - CRON: Será feito um código em python que rodará em batch para pegar os e-mails respondidos pelo esic da Câmara dos Vereadores.





## 09/10/2018
Participantes: Edison e Ricardo
Discussão sobre possíveis soluções para o problema de automatizações para o envio de pedidos de informação à Câmara de Vereadores de São Paulo.

O site da Câmara disponibiliza um formulário para o envio do pedido, porém é necessário passar pelo [reCaptcha](https://www.google.com/recaptcha/intro/v3beta.html) desenvolvida pela Google. Pesquisamos maneiras de burlar ele, porém vimos que era bastante difícil.

Outra alternativa era criarmos um sistema web que basicamente preenchesse automaticamente os campos do formulário e exibisse para o usuário apenas o Captcha por meio de um [iframe](https://developer.mozilla.org/pt-BR/docs/Web/HTML/Element/iframe), porém a injeção de código em um iframe por domínios diferentes (nosso sistema e o formulário da Câmara), é bloqueado pelo navegador por motivos de [segurança](https://en.wikipedia.org/wiki/Same-origin_policy).

Descobrimos uma outra alternativa, que era enviando um e-mail diretamente para a Câmara, informando apenas o Nome, CPF e a descrição da solicitação. Optamos então em desenvolver um sistema que recebe essas informações do usuário e o nosso sistema envia para a Câmara o pedido. Com a resposta da Câmara, nós enviaríamos a resposta para o usuário.