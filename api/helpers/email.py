from __future__ import print_function
from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
from email.mime.text import MIMEText
import base64
import email
import quopri
import os 
dir_path = os.path.dirname(os.path.realpath(__file__))

SCOPES = 'https://mail.google.com/'

def send_email(message):

    store = file.Storage('token.json')
    creds = store.get()
    if not creds or creds.invalid:
        flow = client.flow_from_clientsecrets(dir_path+'/credentials.json', SCOPES)
        creds = tools.run_flow(flow, store)
    service = build('gmail', 'v1', http=creds.authorize(Http()))

    message_text = message

    message = MIMEText(message_text)
    message['to'] = 'edison.bosnyak@gmail.com'
    message['from'] = 'governoaberto18@gmail.com'
    message['subject'] = 'Pedido de informação à Câmara dos Vereadores de São Paulo'
  
    parse = {'raw': base64.urlsafe_b64encode(message.as_string().encode()).decode()}

    message = (service.users().messages().send(userId='me', body=parse)
            .execute())
    print('Message Id: ' + message['id'])

def format_email(author,description,cpf):
    return 'Nome:'+author+'\n'+'CPF:'+cpf+'\n'+'Solicitação:'+description+'\n\n\nAtt.,\n'+author
