import mongoengine
import datetime

class Question(mongoengine.Document):

    author = mongoengine.StringField()
    description = mongoengine.StringField()
    created = mongoengine.DateTimeField(default=datetime.datetime.utcnow)
    answer = mongoengine.StringField()
    status = mongoengine.BooleanField(default=False)
    ticketNumber = mongoengine.StringField()
    cpf = mongoengine.StringField()
    email = mongoengine.StringField()
    meta = {'strict': False}